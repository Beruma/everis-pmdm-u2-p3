package dam2.pmdm.u2.pmdm_p3_beatriz_ruiz_maximo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView resultado;
    EditText numero;
    RadioButton hexS;
    RadioButton hexE;
    RadioButton decS;
    RadioButton decE;
    RadioButton binS;
    RadioButton binE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        resultado = (TextView) findViewById(R.id.salidaNum);
        numero = (EditText) findViewById(R.id.entradaNum);
        hexE = (RadioButton) findViewById(R.id.HexadecimalE);
        hexS = (RadioButton) findViewById(R.id.HexadecimalS);
        binE = (RadioButton) findViewById(R.id.BinarioE);
        binS = (RadioButton) findViewById(R.id.BinarioS);
        decE = (RadioButton) findViewById(R.id.DecimalE);
        decS = (RadioButton) findViewById(R.id.DecimalS);
    }

    //Con este método realiza la función al hacer click sobre el botón comprobando a que método debe llamar para la conversión
    public void conversor(View vista){

        if((hexE.isChecked() && hexS.isChecked())||(decE.isChecked() && decS.isChecked())||(binE.isChecked() && binS.isChecked())) {
            resultado.setText(R.string.presiona_dos_botones_diferentes);
        }else if (hexE.isChecked() && binS.isChecked()){
                HexadecimalABinario(numero);
            } else if (hexE.isChecked() && decS.isChecked()){
                HexadecimalADecimal(numero);
            } else if (binE.isChecked() && hexS.isChecked()){
                BinarioAHexadecimal(numero);
            } else if (binE.isChecked() && decS.isChecked()){
                BinarioADecimal(numero);
            } else if (decE.isChecked() && hexS.isChecked()){
                DecimalAHexadecimal(numero);
            }else if (decE.isChecked() && binS.isChecked()){
                DecimalABinario(numero);
            }
            else{
                resultado.setText(R.string.por_favor_marca_dos_botones);
            }
        }

    //Método para cambiar de hexadecimal a binario
    public void HexadecimalABinario(EditText hex){

        String hexadecimal = hex.getText().toString();
        if (hexadecimal.length()>0) {
            int hexa = Integer.parseInt(hexadecimal, 16);
            String solucion = Integer.toBinaryString(hexa);
            resultado.setText(" " + solucion);
        }else{
            resultado.setText("Debes introducir algo");
        }
    }

    //Método para cambiar de hexadecimal a decimal
    public void HexadecimalADecimal(EditText hex) {

        String hexadecimal = hex.getText().toString();
        if (hexadecimal.length() > 0) {
            int solucion = Integer.parseInt(hexadecimal, 16);
            resultado.setText(" " + solucion);
        } else {
            resultado.setText("Debes introducir algo");
        }
    }

    //Método para cambiar de binario a hexadecimal
    public void BinarioAHexadecimal(EditText bin) {

        String bina = bin.getText().toString();
        if (bina.length() > 0) {
            int binario = Integer.parseInt(bina);
            String solucion = Integer.toHexString(binario);
            resultado.setText(" " + solucion);
        } else {
            resultado.setText("Debes introducir algo");
        }
    }

    //Método para cambiar de binario a decimal
    public void BinarioADecimal(EditText bin) {
        String bina = bin.getText().toString();
        if (bina.length() > 0) {
            int binary = Integer.parseInt(bina, 2);
            String solucion = String.valueOf(binary);
            resultado.setText(" " + solucion);
        } else {
            resultado.setText("Debes introducir algo");
        }
    }

    //Método para cambiar de decimal a hexadecimal
    public void DecimalAHexadecimal(EditText dec) {

        String deci = dec.getText().toString();
        if (deci.length() > 0) {
            int decimal = Integer.parseInt(deci);
            String solucion = Integer.toHexString(decimal);
            resultado.setText(" " + solucion);
        } else {
            resultado.setText("Debes introducir algo");
        }
    }

    //Método para cambiar de decimal a binario
    public void DecimalABinario(EditText dec) {

        String deci = dec.getText().toString();
        if (deci.length() > 0) {
            int decimal = Integer.parseInt(deci);
            String solucion = Integer.toBinaryString(decimal);
            resultado.setText(" " + solucion);
        } else {
            resultado.setText("Debes introducir algo");
        }
    }
}